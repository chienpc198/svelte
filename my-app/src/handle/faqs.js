var coll = document.getElementsByClassName("faqs-item-collapse");
for (var i = 0; i < coll.length; i++) {
    coll[i].addEventListener("click", function() {
        var content = this.nextElementSibling;
        console.log(content)
        if (content.style.display == "block") {
            content.style.display = "none";
            this.style['border-radius'] = '8px';
            this.style.marginBottom = "10px"
        } else {
            content.style.display = "block";
            this.style['border-bottom-left-radius'] = '0px';
            this.style['border-bottom-right-radius'] = '0px';
            this.style.marginBottom = "0px"
            }
    });
}